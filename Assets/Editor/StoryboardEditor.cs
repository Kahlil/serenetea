using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(storyboard))]
public class StoryboardEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector(); // Draws the default inspector

        storyboard storyboardScript = (storyboard)target;
        if (GUILayout.Button("Next Step"))
        {
            storyboardScript.NextStep();
        }
    }
}
